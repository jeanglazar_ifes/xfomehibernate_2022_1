
package dominio;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;


/**
 *
 * @author 1547816
 */

@Entity
public class ItemPedido implements Serializable {
    
    @EmbeddedId
    private ItemPedidoPK chvComposta;
    
    
    int qtde;
    private String observacao;
    private int maisBife;
    private int maisOvo;
    private int maisPresunto;
    private int maisQueijo;
    
    public ItemPedido() {
    }

    public ItemPedido(Lanche lanche, Pedido pedido, int qtde, String observacao, int maisBife, int maisOvo, int maisPresunto, int maisQueijo) {
        this.chvComposta = new ItemPedidoPK(lanche, pedido);
        this.qtde = qtde;
        this.observacao = observacao;
        this.maisBife = maisBife;
        this.maisOvo = maisOvo;
        this.maisPresunto = maisPresunto;
        this.maisQueijo = maisQueijo;
    }
  
    
    
    public int getQtde() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde = qtde;
    }
    
    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getMaisBife() {
        return maisBife;
    }

    public void setMaisBife(int maisBife) {
        this.maisBife = maisBife;
    }

    public int getMaisOvo() {
        return maisOvo;
    }

    public void setMaisOvo(int maisOvo) {
        this.maisOvo = maisOvo;
    }

    public int getMaisPresunto() {
        return maisPresunto;
    }

    public void setMaisPresunto(int maisPresunto) {
        this.maisPresunto = maisPresunto;
    }

    public int getMaisQueijo() {
        return maisQueijo;
    }

    public void setMaisQueijo(int maisQueijo) {
        this.maisQueijo = maisQueijo;
    }

    public ItemPedidoPK getChvComposta() {
        return chvComposta;
    }

    public void setChvComposta(ItemPedidoPK chvComposta) {
        this.chvComposta = chvComposta;
    }
  
    
}
