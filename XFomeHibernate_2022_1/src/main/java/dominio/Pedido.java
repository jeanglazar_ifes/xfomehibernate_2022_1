/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dominio;

import gerenciatarefas.FuncoesUteis;
import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jean_
 */

@Entity
public class Pedido implements Serializable{
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int idPedido;
    
    @Temporal ( TemporalType.TIMESTAMP )
    private Date dataPedido; 
    
    @Column (length = 1)
    private char entregar;
    private float valorTotal;
    
    @ManyToOne ( fetch = FetchType.EAGER)
    @JoinColumn ( name = "idCliente")
    private Cliente cliente;
       
    
    @OneToMany ( mappedBy = "chvComposta.pedido" , cascade = CascadeType.ALL )
    private List<ItemPedido> itensPedido;

    
    public Pedido() {
    }

    public Pedido(Date data, char entregar, float valorTotal, Cliente cliente) {
        this.dataPedido = data;
        this.entregar = entregar;
        this.valorTotal = valorTotal;
        this.cliente = cliente;
        this.itensPedido = new ArrayList();
    }

    public Pedido(int idPedido, Date data, char entregar, float valorTotal, Cliente cliente) {
        this.idPedido = idPedido;
        this.dataPedido = data;
        this.entregar = entregar;
        this.valorTotal = valorTotal;
        this.cliente = cliente;
        this.itensPedido = new ArrayList();
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public Date getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }

    public char getEntregar() {
        return entregar;
    }

    public void setEntregar(char entregar) {
        this.entregar = entregar;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ItemPedido> getItensPedido() {
        return itensPedido;
    }

    public void setItensPedido(List<ItemPedido> itensPedido) {
        this.itensPedido = itensPedido;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.idPedido;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pedido other = (Pedido) obj;
        if (this.idPedido != other.idPedido) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(idPedido);
    }
    
    public Object[] toArray() throws ParseException {   
        
        // FORMATAR MOEDA
        NumberFormat formNum = NumberFormat.getCurrencyInstance();
        
        /*
        // FORMATAR CASAS DECIMAIS 
        DecimalFormat formNum = new DecimalFormat();
        formNum.setMaximumFractionDigits(2);
        formNum.setMinimumFractionDigits(2);
        */
        
        return new Object[] {this, cliente, cliente.getEndereco().getBairro(), 
                             FuncoesUteis.dateToStr(  getDataPedido() ), 
                             formNum.format(valorTotal)  };        
    }
}
