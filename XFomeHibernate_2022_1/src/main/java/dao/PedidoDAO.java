/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import dominio.Pedido;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author 1547816
 */
public class PedidoDAO extends GenericDAO {

        
    private List<Pedido> pesquisar(int tipo, String pesq)  {
                
       List lista = null;
        Session sessao = null;
        try {
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.beginTransaction();

            CriteriaBuilder builder = sessao.getCriteriaBuilder();
            CriteriaQuery consulta = builder.createQuery(Pedido.class);
            
            Root tabela = consulta.from(Pedido.class);
            
            // Mudar o FETCH 
            tabela.fetch("itensPedido", JoinType.INNER);
            
            consulta.distinct(true);
            
            Predicate restricoes = null;
            switch ( tipo ) {
                case 0: restricoes = builder.equal(tabela.get("idPedido"), pesq);
                        break;      
                case 1: restricoes = builder.like(tabela.get("cliente").get("nome"), pesq+"%");
                        break;      
                                        
                case 2: restricoes = builder.like(tabela.get("cliente").get("endereco").get("bairro"), pesq+"%");
                        break;                    
                    
                case 3: String vetor [] = pesq.split("/");
                        Expression exprMes = builder.function("month", Integer.class, tabela.get("dataPedido") );
                        Expression exprAno = builder.function("year", Integer.class, tabela.get("dataPedido") );
                        restricoes = builder.and(
                                        builder.equal(exprMes, vetor[0]),
                                        builder.equal(exprAno, vetor[1])                            
                                );
                        break;
            }

            consulta.where(restricoes);
            // EXECUTAR
            lista = sessao.createQuery(consulta).getResultList();

            sessao.getTransaction().commit();
            sessao.close();
        } catch (HibernateException ex) {
            if (sessao != null ) {
                sessao.getTransaction().rollback();          
                sessao.close();
            }
            throw new HibernateException(ex);
        }
        return lista;
                                        
    }
    
    
    public List<Pedido> pesquisarPorID(String pesq) {
         return pesquisar(0,pesq);             
    }
    
    public List<Pedido> pesquisarPorCliente(String pesq) {             
        return pesquisar(1,pesq);
    }
    
    public List<Pedido> pesquisarPorBairro(String pesq) {             
        return pesquisar(2,pesq);
    }
        
    public List<Pedido> pesquisarPorMes(String pesq) {             
        return pesquisar(3,pesq);
    }
    
    
    
    public List valorPorCliente()  {
                
        List lista = null;
        Session sessao = null;
        try {
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.beginTransaction();

            CriteriaBuilder builder = sessao.getCriteriaBuilder();
            CriteriaQuery consulta = builder.createQuery(Object[].class);
            Root tabela = consulta.from(Pedido.class);
            
            consulta.multiselect( tabela.get("cliente"), builder.sum( tabela.get("valorTotal"))  );
            consulta.groupBy( tabela.get("cliente") );

            // EXECUTAR
            lista = sessao.createQuery(consulta).getResultList();

            sessao.getTransaction().commit();
            sessao.close();
        } catch (HibernateException ex) {
            if (sessao != null ) {
                sessao.getTransaction().rollback();          
                sessao.close();
            }
            throw new HibernateException(ex);
        }
        return lista;
    }

    
    public List valorPorMes()  {
                
        List lista = null;
        Session sessao = null;
        try {
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.beginTransaction();

            CriteriaBuilder builder = sessao.getCriteriaBuilder();
            CriteriaQuery consulta = builder.createQuery(Object[].class);
            Root tabela = consulta.from(Pedido.class);
            
            Expression exprMes = builder.function("month", Integer.class, tabela.get("dataPedido") );
            Expression exprAno = builder.function("year", Integer.class, tabela.get("dataPedido") );
            
            consulta.multiselect( exprMes, exprAno, builder.sum( tabela.get("valorTotal"))  );
            consulta.groupBy( exprMes, exprAno );

            // EXECUTAR
            lista = sessao.createQuery(consulta).getResultList();

            sessao.getTransaction().commit();
            sessao.close();
        } catch (HibernateException ex) {
            if (sessao != null ) {
                sessao.getTransaction().rollback();          
                sessao.close();
            }
            throw new HibernateException(ex);
        }
        return lista;
    }

}



/*

################################################################

  CRITERIA ANTIGO
  -----------------

  Criteria consulta = sessao.createCriteria(Pedido.class);
            consulta.setFetchMode("itensPedido", FetchMode.JOIN);
            consulta.setResultTransformer( Criteria.DISTINCT_ROOT_ENTITY );
            
            consulta.addOrder( Order.asc("idPedido") );
            
            switch (tipo) {
                case 0: consulta.add( Restrictions.eq("idPedido", Integer.parseInt(pesq)));
                        break;
                        
                case 1: consulta.createAlias("cliente", "cli");
                        consulta.add( Restrictions.like("cli.nome", pesq+"%"));
                        break;
                        
                case 2: consulta.createAlias("cliente", "cli");
                        consulta.createAlias("cli.endereco", "ender");
                        consulta.add( Restrictions.like("ender.bairro", pesq+"%"));
                        break;
                        
                case 3: String vetor[] = pesq.split("/");
                        consulta.add( Restrictions.sqlRestriction("MONTH(dataPedido) = " + vetor[0] 
                                + " and YEAR(dataPedido) = " + vetor[1]) );
                        break; 
            }                           
            lista = consulta.list();

###############################################################

           // CRITERIA ANTIGO
            Criteria cons = sessao.createCriteria(Pedido.class);

            // Definir o PROJECTION
            cons.setProjection( Projections.projectionList()
                    .add( Projections.sum("valorTotal"))
                    .add(Projections.sqlGroupProjection(
                        "YEAR(dataPedido) as ano, MONTH(dataPedido) as mes",
                        "ano, mes",
                        new String[] {"ano", "mes"},
                        new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING  }
                    ))
            );

            lista = cons.list();

##########################################



*/