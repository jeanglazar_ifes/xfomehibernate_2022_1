/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dominio.Cliente;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author 1547816
 */
public class ClienteDAO extends GenericDAO {

        
    private List<Cliente> pesquisar(String pesq, int tipo)  {
                
        List lista = null;
        Session sessao = null;
        try {
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.beginTransaction();

            CriteriaBuilder builder = sessao.getCriteriaBuilder();
            CriteriaQuery consulta = builder.createQuery(Cliente.class);
            
            Root tabela = consulta.from(Cliente.class);
            
            Predicate restricoes = null;
            switch ( tipo ) {
                case 1: restricoes = builder.like(tabela.get("nome"), pesq+"%");
                        break;      
                                        
                case 2: restricoes = builder.like(tabela.get("endereco").get("bairro"), pesq+"%");
                        break;                    
                    
                case 3: Expression expr = builder.function("month", Integer.class, tabela.get("dtNasc") );
                        restricoes = builder.equal(expr, pesq);
                        break;
            }

            consulta.where(restricoes);
            // EXECUTAR
            lista = sessao.createQuery(consulta).getResultList();

            sessao.getTransaction().commit();
            sessao.close();
        } catch (HibernateException ex) {
            if (sessao != null ) {
                sessao.getTransaction().rollback();          
                sessao.close();
            }
            throw new HibernateException(ex);
        }
        return lista;
    }
                                       
    
    
    public List<Cliente> pesquisarPorNome(String pesq) throws ClassNotFoundException, SQLException {
        return pesquisar(pesq, 1);
    }
    
    public List<Cliente> pesquisarPorBairro(String pesq) throws ClassNotFoundException, SQLException {
        return pesquisar(pesq, 2);
    }
    
    public List<Cliente> pesquisarPorMes(String pesq) throws ClassNotFoundException, SQLException {
        return pesquisar(pesq, 3);
    }

    
     public List contPorBairro()  {
                
        List lista = null;
        Session sessao = null;
        try {
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.beginTransaction();

            CriteriaBuilder builder = sessao.getCriteriaBuilder();
            CriteriaQuery consulta = builder.createQuery(Object[].class);
            Root tabela = consulta.from(Cliente.class);
            
            consulta.multiselect( tabela.get("endereco").get("bairro"), builder.count( tabela.get("idCliente"))  );
            consulta.groupBy( tabela.get("endereco").get("bairro") );

            // EXECUTAR
            lista = sessao.createQuery(consulta).getResultList();

            sessao.getTransaction().commit();
            sessao.close();
        } catch (HibernateException ex) {
            if (sessao != null ) {
                sessao.getTransaction().rollback();          
                sessao.close();
            }
            throw new HibernateException(ex);
        }
        return lista;
    }

}



/*

################################################################
  
  CRITERIA ANTIGO
  -----------------
  Criteria consulta = sessao.createCriteria(Cliente.class);
            switch (tipo) {
                case 1: consulta.add( Restrictions.like("nome", pesq+"%") );
                        break;
                case 2: consulta.createAlias("endereco", "ender");
                        consulta.add ( Restrictions.like("ender.bairro", pesq+"%") );
                        break;
                case 3: consulta.add( Restrictions.sqlRestriction("MONTH(dtNasc) = " + pesq) );
            }                           
            lista = consulta.list();

###############################################################


            Criteria cons = sessao.createCriteria(Cliente.class);
            cons.createAlias("endereco", "ender");

            // Definir o PROJECTION
            cons.setProjection( Projections.projectionList()
                    .add( Projections.count("idCliente") )
                    .add( Projections.groupProperty("ender.bairro"))
            );

            lista = cons.list();

########################

            CriteriaBuilder builder = sessao.getCriteriaBuilder();
            CriteriaQuery consulta = builder.createQuery(Object[].class);
            Root tabela = consulta.from(Cliente.class);
            
            
            consulta.multiselect(  tabela.get("endereco").get("bairro"),  builder.count(tabela.get("idCliente")));
            consulta.groupBy( tabela.get("endereco").get("bairro") );

           
            // EXECUTAR
            lista = sessao.createQuery(consulta).getResultList();
            
*/