/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gerenciatarefas;

import dao.ClienteDAO;
import dao.ConexaoHibernate;
import dao.GenericDAO;
import dao.PedidoDAO;
import dominio.Cidade;
import dominio.Cliente;
import dominio.ItemPedido;
import dominio.Lanche;
import dominio.Pedido;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author 1547816
 */
public class GerenciadorDominio {

    private GenericDAO genDao;
    private ClienteDAO cliDao;
    private PedidoDAO pedDao;
    
    public GerenciadorDominio() throws ClassNotFoundException, SQLException {
    
        // ABRIR A CONEXAO HIBERNATE
        ConexaoHibernate.getSessionFactory();
 
        genDao = new GenericDAO();
        cliDao = new ClienteDAO();
        pedDao = new PedidoDAO();
    }
    
    
    // FUNÇÃO GENÉRICA
    public List listar(Class classe) throws ClassNotFoundException, SQLException {
        return genDao.listar( classe );
        
    }

    
    public int inserirCliente(String nome, String cpf, Date dtNasc, char sexo,
            String cep, String ender, int num, String complemento, String bairro,
            String referencia, String telFixo, String celular, String email, Icon foto, Cidade cidade) throws ClassNotFoundException, SQLException  {

            Cliente cli = new Cliente(nome, cpf, dtNasc, sexo, cep, ender, num, 
                    complemento, bairro, referencia, telFixo, celular, email, 
                    FuncoesUteis.IconToBytes(foto), cidade);
            
            cliDao.inserir(cli);
            return cli.getIdCliente();
        
    }
    
    public void alterarCliente(Cliente cli, String nome, String cpf, Date dtNasc, char sexo,
        String cep, String ender, int num, String complemento, String bairro,
        String referencia, String telFixo, String celular, String email, Icon foto, Cidade cidade) throws ClassNotFoundException, SQLException  {

//        Cliente cli = new Cliente(nome, cpf, dtNasc, sexo, cep, ender, num, 
  //              complemento, bairro, referencia, telFixo, celular, email, 
    //            FuncoesUteis.IconToBytes(foto), cidade);

    //    cli.setIdCliente(idCliente);
    
        cli.setNome(nome);
        cli.setCpf(cpf);
        cli.setDtNasc(dtNasc);
        cli.setSexo(sexo);
        cli.getEndereco().setCep(cep);
        cli.getEndereco().setLogradouro(ender);
        cli.getEndereco().setNumero(num);
        cli.getEndereco().setComplemento(complemento);
        cli.getEndereco().setBairro(bairro);
        cli.getEndereco().setReferencia(referencia);
        cli.setTelFixo(telFixo);
        cli.setCelular(celular);
        cli.setEmail(email);
        cli.setFoto(FuncoesUteis.IconToBytes(foto));
        cli.setCidade(cidade);
        
        cliDao.alterar(cli);            
        
    }
    
    // FUNÇÃO GENÉRICA
    public void excluir (Object obj) throws ClassNotFoundException, SQLException {
        genDao.excluir(obj);            
    }
    
        
    public List<Cliente> pesquisarCliente(String pesq, int tipo) throws ClassNotFoundException, SQLException {
        
        switch (tipo) {
            case 0: return cliDao.pesquisarPorNome(pesq);
            case 1: return cliDao.pesquisarPorBairro(pesq);
            case 2: return cliDao.pesquisarPorMes(pesq);
            default: return null;
        }
        
    }
    
    
    public int inserirPedido(Cliente cli, char entrega, JTable tblPedidos) {
        Pedido ped = new Pedido(new Date(), entrega, 0, cli);
        float valorTotal = 0;
        
        List<ItemPedido> lista = ped.getItensPedido();
        
        int tam = tblPedidos.getRowCount();
        if ( tam > 0 ) {
            for(int lin=0; lin < tam; lin++) {
                int col = 0;
                Lanche lanche = (Lanche) tblPedidos.getValueAt(lin, col++);
                int qtde = (int) tblPedidos.getValueAt(lin, col++);
                int maisBife = (int) tblPedidos.getValueAt(lin, col++);
                int maisQueijo = (int) tblPedidos.getValueAt(lin, col++);
                int maisPresunto = (int) tblPedidos.getValueAt(lin, col++);
                int maisOvo = (int) tblPedidos.getValueAt(lin, col++);
                String observacao = tblPedidos.getValueAt(lin, col++).toString();
                
                ItemPedido item = new ItemPedido(lanche, ped, qtde, observacao, maisBife, maisOvo, maisPresunto, maisQueijo);
                lista.add(item);
                valorTotal = valorTotal + lanche.getValor() * qtde;
            } 
            ped.setValorTotal(valorTotal);
            genDao.inserir(ped);
            return ped.getIdPedido();
        } else {
           return -1;
        }
 
    }
    
    
    public List<Pedido> pesquisarPedido(int tipo, String pesq) {
        List<Pedido> lista = null;

        switch (tipo) {
            case 0: 
                // Verificar se o ID é inteiro
                int id = Integer.parseInt(pesq);
                lista = pedDao.pesquisarPorID(pesq);
                break;
            case 1:
                lista = pedDao.pesquisarPorCliente(pesq);
                break;                
            case 2:
                lista = pedDao.pesquisarPorBairro(pesq);
                break;
            case 3:
                // Verificar se está no formato MM/YYYY
                String vetor[] = pesq.split("/");
                if ( vetor.length == 2 ) {
                    lista = pedDao.pesquisarPorMes(pesq);
                } else {
                    throw new NumberFormatException("Usar o formato MÊS/ANO: MM/YYYY");
                }
                break;

        }
        
        return lista;
    }

     public void relGroupBy(JTable tabela, int tipo) throws Exception {
        List<Object[]> lista = null;
        Cliente cli = null;

        // Limpa a tabela
        ((DefaultTableModel) tabela.getModel()).setRowCount(0);

        switch (tipo) {
            case 'B':
                lista = cliDao.contPorBairro();
                break;
            case 'M':
                lista = pedDao.valorPorMes();
                break;
            case 'C':
                lista = pedDao.valorPorCliente();
                break;
        }

        NumberFormat formato = NumberFormat.getCurrencyInstance();
        // Percorrer a LISTA
        if (lista != null) {

 
            for (Object[] obj : lista) {
                switch (tipo) {
                    case 'M':                       
                        obj[0] = obj[0].toString() + "/" + obj[1].toString();                        
                        obj[1] = formato.format( Double.parseDouble( obj[2].toString() ));
                        break;
                    case 'C':
                        obj[1] = formato.format( Double.parseDouble( obj[1].toString() ) );
                        break;
                        
                }

                ((DefaultTableModel) tabela.getModel()).addRow(obj);
            }

        }
    }
}
