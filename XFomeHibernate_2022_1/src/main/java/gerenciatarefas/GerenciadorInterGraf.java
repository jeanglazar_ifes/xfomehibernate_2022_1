/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gerenciatarefas;

import dominio.Cidade;
import dominio.Cliente;
import dominio.Pedido;
import intergraf.DlgCadCliente;
import intergraf.DlgCadPedido;
import intergraf.DlgFerramentas;
import intergraf.DlgPesqCliente;
import intergraf.DlgPesqPedido;
import intergraf.DlgRelGroupBy;
import intergraf.FrmPrincipal;
import java.awt.Frame;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author 1547816
 */
public class GerenciadorInterGraf {
    
    private FrmPrincipal janPrinc;
    private DlgCadCliente janCadCliente;
    private DlgCadPedido janCadPedido;
    private DlgFerramentas janFer;
    private DlgPesqCliente janPesqCli;
    private DlgPesqPedido janPesqPed;
    private DlgRelGroupBy janGroupBy;
    
    GerenciadorDominio gerDom;

    public GerenciadorInterGraf() {
        try {
            gerDom = new GerenciadorDominio();
            janPrinc = null;
            janCadCliente = null;
            janCadPedido = null;
            janFer = null;
            janPesqCli = null;
            janPesqPed = null;
            janGroupBy = null;
            
        } catch (ClassNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(janPrinc, 
                    "Erro ao conectar com o banco de dados." + ex,
                    "Erro conexão", JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
    
    }

    public GerenciadorDominio getGerDom() {
        return gerDom;
    }
    
    
   
    
    private JDialog abrirJanela(java.awt.Frame parent, JDialog dlg, Class classe){
        if (dlg == null){     
            try {
                dlg = (JDialog) classe.getConstructor(Frame.class, boolean.class, GerenciadorInterGraf.class ).newInstance(parent,true,this);
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                JOptionPane.showMessageDialog(janPrinc, "Erro ao abrir a janela " + classe.getName() );
            }
        }        
        dlg.setVisible(true); 
        return dlg;
    }
    
    public void janPrincipal() {
        if ( janPrinc == null ) {
            janPrinc = new FrmPrincipal(this);
        }
        janPrinc.setVisible(true);
    }    
    
    public void janCadCliente() {
        janCadCliente = (DlgCadCliente) abrirJanela(janPrinc, janCadCliente, DlgCadCliente.class );
    }
        
    public void janCadPedido() {
        janCadPedido = (DlgCadPedido) abrirJanela(janPrinc, janCadPedido, DlgCadPedido.class );
    }
    
    public void janFerramentas() {
        janFer = (DlgFerramentas) abrirJanela(janPrinc, janFer, DlgFerramentas.class );
    }
    
    public Cliente janPesqCliente() {
        janPesqCli = (DlgPesqCliente) abrirJanela(janPrinc, janPesqCli, DlgPesqCliente.class );
        return janPesqCli.getCliente();
    }
    
    public Pedido janPesqPedido(){
        janPesqPed = (DlgPesqPedido) abrirJanela(janPrinc, janPesqPed, DlgPesqPedido.class);
        return janPesqPed.getPedidoSelecionado();
    }
    
    public void janGroupBy(){
        janGroupBy = (DlgRelGroupBy) abrirJanela(janPrinc, janGroupBy, DlgRelGroupBy.class);
    }
    
    public void carregarCombo(JComboBox combo, Class classe) {
        List lista;
        try {
            lista = gerDom.listar(classe);
            combo.setModel( new DefaultComboBoxModel( lista.toArray() )  );
        } catch (ClassNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(janPrinc, 
                    "Erro ao carregar as cidades." + ex,
                    "Erro cidade", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    
    /*
    public void carregarComboCidade(JComboBox combo) {
        List<Cidade> lista;
        try {
            lista = gerDom.listarCidades();
            combo.setModel( new DefaultComboBoxModel( lista.toArray() )  );
        } catch (ClassNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(janPrinc, 
                    "Erro ao carregar as cidades." + ex,
                    "Erro cidade", JOptionPane.ERROR_MESSAGE);
        }
    }
    */
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        GerenciadorInterGraf gerIG = new GerenciadorInterGraf();
        gerIG.janPrincipal();
    }
    
    
}
