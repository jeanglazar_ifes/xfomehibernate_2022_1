
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


/**
 *
 * @author 1547816
 */

@Entity
public class Lanche implements Serializable {
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int idLanche;  
    
    private String nome;
    private float valor;
    private String ingredientes;

    
    @OneToMany (mappedBy = "chvComposta.lanche")
    private List<ItemPedido> itensPedido;

    
    public Lanche() {
    }
   
    
    public Lanche(String nome, float valor, String ingredientes) {
        this.nome = nome;
        this.valor = valor;
        this.ingredientes = ingredientes;
    }

    public int getIdLanche() {
        return idLanche;
    }

    public void setIdLanche(int idLanche) {
        this.idLanche = idLanche;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }
    
    @Override
    public String toString() {
        return nome + "-" + valor;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + this.idLanche;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Lanche other = (Lanche) obj;
        if (this.idLanche != other.idLanche) {
            return false;
        }
        return true;
    }

}
